#!/bin/bash
source /etc/profile.d/conda.sh
conda activate utg-evr-03
caput -a LabS-Utgard-VIP:TS-EVR-3:SoftSeq-0-EvtCode-SP 2 31 127
caput -a LabS-Utgard-VIP:TS-EVR-3:SoftSeq-0-Timestamp-SP 2 0 1
