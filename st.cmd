#!/usr/bin/env iocsh.bash

################################################################
### requires
require(mrfioc2)
require(evr_seq_calc)
require(cntpstats)
require(ntpshm)
require(ptpstats)

epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")

iocshLoad("./iocsh/env-init.iocsh")

epicsEnvSet("SYS", "LabS-Utgard-VIP:")
epicsEnvSet("DEV", "TS-EVR-3")
epicsEnvSet("EVR", "$(DEV)")
epicsEnvSet("SYSPV",        "$(SYS)$(DEV)")
epicsEnvSet("CHOP_DRV", "Chop-Drv-01")
epicsEnvSet("CHOP_SYS",     "$(SYS)")
epicsEnvSet("CHOP_EVR",     "$(DEV):")
epicsEnvSet("CHOP_CHIC",    "Chop-CHIC-01:")
epicsEnvSet("CHOP_DRV1",    "Chop-Eld-01:")
epicsEnvSet("CHOP_DRV2",    "Chop-Eld-02:")
epicsEnvSet("CHOP_DRV3",    "Chop-Eld-03:")
epicsEnvSet("CHOP_DRV4",    "Chop-Eld-04:")
epicsEnvSet("BASEEVTNO",    "25")
epicsEnvSet("MRF_HW_DB", "evr-pcie-300dc-univ.db")
epicsEnvSet("PCIID", "1:0.0")


# NTP monitor
iocshLoad("$(cntpstats_DIR)/cntpstats.iocsh","SYS=$(SYS), DEV=$(DEV):")

# E3 Common databases
iocshLoad("$(essioc_DIR)/common_config.iocsh")

iocshLoad("$(mrfioc2_DIR)/evr.iocsh", "EVRDB=$(MRF_HW_DB), P=$(SYSPV), EVR=EVR, PCIID=$(PCIID)")

iocshLoad("$(mrfioc2_DIR)/evrevt.iocsh", "P=$(SYSPV)")

dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":00-", EVR=EVR, CODE=21, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")
dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":01-", EVR=EVR, CODE=22, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")
dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":02-", EVR=EVR, CODE=23, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")
dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":03-", EVR=EVR, CODE=24, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")


# Load the sequencer configuration script
iocshLoad("$(evr_seq_calc_DIR)/evr_seq_calc.iocsh", "P=$(CHOP_SYS), R1=$(CHOP_DRV1), R2=$(CHOP_DRV2), R3=$(CHOP_DRV3), R4=$(CHOP_DRV4), EVR=$(CHOP_EVR), BASEEVTNO=$(BASEEVTNO)")

# Configure the EVR to write its timestamp into NTP shared memory segment 2
# and load some records to expose the NTP shared memory segment as PVs
time2ntp("EVR", 2)
iocshLoad("$(ntpshm_DIR)/ntpshm.iocsh", "P=$(SYS), R=$(DEV):")

# turn on mrfioc2 time debugging messages
var evrMrmTimeDebug 1

# PTP status PVs
iocshLoad("$(ptpstats_DIR)/ptpstats.iocsh", "P=$(SYS), R=$(DEV):")

iocInit()

iocshLoad("$(mrfioc2_DIR)/evr.r.iocsh", "P=$(SYSPV), EVR=EVR")

######### INPUTS #########
# Trig-Ext-Sel changed from "Off" to "Edge", Code-Ext-SP changed from 0 to 10
dbpf $(SYSPV):UnivIn-0-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-0-Edge-Sel "Active Rising"
dbpf $(SYSPV):Out-RB00-Src-SP 61
dbpf $(SYSPV):UnivIn-0-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-0-Trig-Back-Sel "Off"
dbpf $(SYSPV):UnivIn-0-Code-Ext-SP 21
dbpf $(SYSPV):UnivIn-0-Code-Back-SP 0

dbpf $(SYSPV):UnivIn-1-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-1-Edge-Sel "Active Rising"
dbpf $(SYSPV):Out-RB01-Src-SP 61
dbpf $(SYSPV):UnivIn-1-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-1-Trig-Back-Sel "Off"
dbpf $(SYSPV):UnivIn-1-Code-Ext-SP 22
dbpf $(SYSPV):UnivIn-1-Code-Back-SP 0

dbpf $(SYSPV):UnivIn-2-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-2-Edge-Sel "Active Rising"
dbpf $(SYSPV):Out-RB02-Src-SP 61
dbpf $(SYSPV):UnivIn-2-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-2-Trig-Back-Sel "Off"
dbpf $(SYSPV):UnivIn-2-Code-Ext-SP 23
dbpf $(SYSPV):UnivIn-2-Code-Back-SP 0

dbpf $(SYSPV):UnivIn-3-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-3-Edge-Sel "Active Rising"
dbpf $(SYSPV):Out-RB03-Src-SP 61
dbpf $(SYSPV):UnivIn-3-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-3-Trig-Back-Sel "Off"
dbpf $(SYSPV):UnivIn-3-Code-Ext-SP 24
dbpf $(SYSPV):UnivIn-3-Code-Back-SP 0


dbpf $(SYSPV):EvtA-SP.OUT "@OBJ=EVR,Code=14" 
dbpf $(SYSPV):EvtA-SP.VAL 14 
dbpf $(SYSPV):EvtB-SP.OUT "@OBJ=EVR,Code=21" 
dbpf $(SYSPV):EvtB-SP.VAL 21 
dbpf $(SYSPV):EvtC-SP.OUT "@OBJ=EVR,Code=22" 
dbpf $(SYSPV):EvtC-SP.VAL 22
dbpf $(SYSPV):EvtD-SP.OUT "@OBJ=EVR,Code=23"
dbpf $(SYSPV):EvtD-SP.VAL 23
dbpf $(SYSPV):EvtE-SP.OUT "@OBJ=EVR,Code=125"
dbpf $(SYSPV):EvtE-SP.VAL 125
dbpf $(SYSPV):EvtF-SP.OUT "@OBJ=EVR,Code=31"
dbpf $(SYSPV):EvtF-SP.VAL 31
dbpf $(SYSPV):EvtG-SP.OUT "@OBJ=EVR,Code=26"
dbpf $(SYSPV):EvtG-SP.VAL 26
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=EVR,Code=27"
dbpf $(SYSPV):EvtH-SP.VAL 27
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=EVR,Code=28"
dbpf $(SYSPV):EvtH-SP.VAL 28
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=EVR,Code=29"
dbpf $(SYSPV):EvtH-SP.VAL 29
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=EVR,Code=30"
dbpf $(SYSPV):EvtH-SP.VAL 30


######### OUTPUTS #########
######### OUTPUTS #########
dbpf $(SYSPV):DlyGen-0-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-0-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-0-Evt-Trig0-SP 14

dbpf $(SYSPV):DlyGen-1-Evt-Trig0-SP 125
dbpf $(SYSPV):DlyGen-1-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-1-Delay-SP 500000 #0ms
dbpf $(SYSPV):Out-RB07-Src-SP 1

#Set up delay generator 2 to trigger on event 25 and output 4
dbpf $(SYSPV):DlyGen-2-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-2-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-2-Evt-Trig0-SP 14

dbpf $(SYSPV):DlyGen-3-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-3-Delay-SP 500000 #0ms
dbpf $(SYSPV):DlyGen-3-Evt-Trig0-SP 125

dbpf $(SYSPV):DlyGen-4-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-4-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-4-Evt-Trig0-SP 31
dbpf $(SYSPV):Out-RB04-Src-SP 4
dbpf $(SYSPV):Out-RB06-Src-SP 4

dbpf $(SYSPV):DlyGen-5-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-5-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-5-Evt-Trig0-SP 31
dbpf $(SYSPV):Out-RB05-Src-SP 5


######## Sequencer #########
#dbpf $(SYSPV):Base-Freq 14.00000064
#dbpf $(SYSPV):End-Event-Ticks 4
dbpf $(SYSPV):PS-1-Div-SP 8805250

# Load sequencer setup
dbpf $(SYSPV):SoftSeq-0-Load-Cmd 1

# Enable sequencer
dbpf $(SYSPV):SoftSeq-0-Enable-Cmd 1

# Select run mode, "Single" needs a new Enable-Cmd every time, "Normal" needs Enable-Cmd once
dbpf $(SYSPV):SoftSeq-0-RunMode-Sel "Normal"

# Load sequence events and corresponding tick lists
system "/bin/bash /opt/iocs/utg-evr-03/conf_evr_seq.sh"

# Use ticks or microseconds
dbpf $(SYSPV):SoftSeq-0-TsResolution-Sel "Ticks"

# Select trigger source for soft seq 0, trigger source 0, delay gen 0
dbpf $(SYSPV):SoftSeq-0-TrigSrc-2-Sel "Prescaler 1"

# Commit all the settings for the sequnce
# commit-cmd by evrseq!!! 
epicsThreadSleep 2

dbpf $(SYSPV):SoftSeq-0-Commit-Cmd "1"
